{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Unsafe #-}

module Main (main) where

import Data.Graph.Arc (Arc(..))
import Data.Graph.DG (vertexPresentIn)
import Data.Graph.RDAG ( RDAG, ValidationError(..), addArc, dg, empty )
import Data.Text (Text, pack, words, unpack)
import Prelude hiding (words)

import System.Console.Haskeline
import Control.Monad (unless)

-- TODO: Parse quotes
parseQuotedWords :: Text -> Maybe [Text]
parseQuotedWords = return . words

loop :: RDAG Text -> InputT IO ()
loop rdag = do
    mLine <- getInputLine "> "
    case mLine of
        Nothing -> return ()
        Just "quit" -> return ()
        Just line -> case parseQuotedWords $ pack line of
            Nothing -> do
                outputStrLn "Parse error"
                loop rdag
            Just ["add"] -> do
                outputStrLn "Usage: add <source-vertex> <target-vertex>"
                loop rdag
            Just ["add", _] -> do
                outputStrLn "Expected target vertex"
                outputStrLn "Usage: add <source-vertex> <target-vertex>"
                loop rdag
            Just ["add", sourceVertex, targetVertex] -> do
                addCmd sourceVertex targetVertex
            _ -> do
                outputStrLn "Unrecognised command"
                loop rdag
    where
        addCmd :: Text -> Text -> InputT IO ()
        addCmd sourceVertex targetVertex = case addArc rdag $ Arc sourceVertex targetVertex of
            Left Redundant -> do
                outputStrLn "Not adding this arc because it would be redundant"
                loop rdag
            Left Cycle -> do
                outputStrLn "Error: this arc would create a cycle in the graph"
                loop rdag
            Right rdag' -> do
                noteIfVertexAbsent sourceVertex
                noteIfVertexAbsent targetVertex
                loop rdag'
        noteIfVertexAbsent :: Text -> InputT IO ()
        noteIfVertexAbsent vertex =
            unless (vertex `vertexPresentIn` dg rdag) $ outputStrLn $ "Note: vertex '" ++ unpack vertex ++ "' has been added to the DAG"

main :: IO ()
main = runInputT defaultSettings $ loop empty