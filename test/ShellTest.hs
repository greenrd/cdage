module Main where

import           System.Exit    ( exitWith )
-- Based on code from https://github.com/simonmichael/shelltestrunner/issues/11
import           System.Process ( createProcess, shell, waitForProcess )

-- Get a CreateProcess object corresponding to our shell command.
createproc = shell "shelltest -p -c test/shell/*.test"

main :: IO ()
main = do
    -- Ignore stdin/stdout/stderr...
    (_, _, _, hproc) <- createProcess createproc
    -- Now run the ProcessHandle and exit with its result.
    result <- waitForProcess hproc
    exitWith result