{-# LANGUAGE Safe #-}
{-# LANGUAGE TupleSections #-}

module Data.Multimap.Utils ( mkSetMultimap, ReflexiveSetMap, presentIn ) where

import           Control.Monad     ( (<=<) )

import           Data.Multimap.Set ( SetMultimap )
import qualified Data.Multimap.Set as SetMultimap
import           Data.Set          ( Set )
import qualified Data.Set          as Set

dotProduct :: (Ord k, Ord v) => (k, Set v) -> Set (k, v)
dotProduct (x, s) = Set.map (x, ) s

mkSetMultimap :: (Ord k, Ord v) => [(k, Set v)] -> SetMultimap k v
mkSetMultimap = SetMultimap.fromList . (Set.elems <=< map dotProduct)

type ReflexiveSetMap v = SetMultimap v v

presentIn :: Ord v => v -> ReflexiveSetMap v -> Bool
presentIn x sm = x `SetMultimap.member` sm || x `elem` SetMultimap.elems sm