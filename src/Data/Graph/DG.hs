{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE Unsafe #-}

module Data.Graph.DG
    ( DG
    , arcs
    , arcsLens
    , empty
    , extraVertices
    , extraVerticesLens
    , presentIn
    , normalize
    , reverse
    , addArc
    , vertexPresentIn
    ) where

import           Control.Lens.Lens      ( Lens', lens )
import           Control.Lens.Operators ( (%~), (&) )

import           Data.Function          ( fix )
import           Data.Graph.Arc         ( Arc(..) )
import qualified Data.Multimap.Set      as SetMultimap
import           Data.Multimap.Utils    ( ReflexiveSetMap, presentIn )
import           Data.Set               ( Set )
import qualified Data.Set               as Set
import           Data.Tuple             ( swap )

import           Prelude                hiding ( reverse )

-- | Directed Graph
data DG v = DG { arcs :: ReflexiveSetMap v, extraVertices :: Set v, reverse :: DG v }

mkDG :: Ord v => ReflexiveSetMap v -> Set v -> DG v
mkDG as evs = fix $ \me ->
    DG { arcs          = as
       , extraVertices = evs
       , reverse       =
             DG { arcs = SetMultimap.fromList . map swap $ SetMultimap.assocs as, extraVertices = evs, reverse = me }
       }

empty :: Ord v => DG v
empty = mkDG SetMultimap.empty Set.empty

arcsLens :: Ord v => Lens' (DG v) (ReflexiveSetMap v)
arcsLens = lens arcs $ \DG{..} -> (`mkDG` extraVertices)

extraVerticesLens :: Ord v => Lens' (DG v) (Set v)
extraVerticesLens = lens extraVertices $ \DG{..} -> mkDG arcs

vertexPresentIn :: Ord v => v -> DG v -> Bool
vertexPresentIn vertex dg = vertex `presentIn` arcs dg

-- | Normalizes a DG so that 'extraVertices' contains only the vertices not present in 'arcs'
normalize :: Ord v => DG v -> DG v
normalize dg = dg & extraVerticesLens %~ Set.filter (not . (`vertexPresentIn` dg))

addArc :: Ord v => DG v -> Arc v -> DG v
addArc dg Arc{..} = dg & arcsLens %~ SetMultimap.insert source target