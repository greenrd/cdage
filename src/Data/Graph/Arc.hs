{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE Safe #-}

module Data.Graph.Arc ( Arc(..), reverse ) where

import           Prelude hiding ( reverse )

data Arc v = Arc { source :: v, target :: v }

reverse :: Arc v -> Arc v
reverse Arc{..} = Arc { source = target, target = source }