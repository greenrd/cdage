{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE Unsafe #-}

-- | Reduced Directed Acyclic Graphs - DAGs that are in transitively-reduced form
module Data.Graph.RDAG ( RDAG, ValidationError(..), addArc, dg, empty ) where

import           Control.Lens.Operators ( (%~), (&) )

import           Data.Function          ( fix )
import           Data.Graph.Arc         ( Arc(..) )
import qualified Data.Graph.Arc         as Arc
import           Data.Graph.DG          ( DG, arcs, arcsLens )
import qualified Data.Graph.DG          as DG
import qualified Data.Multimap.Set      as SetMultimap
import           Data.Multimap.Utils    ( mkSetMultimap )
import           Data.Set               ( Set )
import qualified Data.Set               as Set

data RDAG v = RDAG { dg :: DG v, transitiveClosure :: RDAG v, descendants :: v -> Set v, ancestors :: v -> Set v }

mkRDAG :: Ord v => DG v -> RDAG v
mkRDAG g = fix $ \me ->
    let tc = RDAG { dg = g & arcsLens %~ (mkSetMultimap . map (\s -> (s, reachableSet me s)) . SetMultimap.keys)
                  , transitiveClosure = tc
                  , descendants = children tc
                  , ancestors = children $ fix $ \rtc ->
                        RDAG { dg = DG.reverse (dg tc)
                             , transitiveClosure = rtc
                             , descendants = ancestors tc
                             , ancestors = descendants tc
                             }
                  }
    in
        RDAG { dg = g, transitiveClosure = tc, descendants = descendants tc, ancestors = ancestors tc }

empty :: Ord v => RDAG v
empty = mkRDAG DG.empty

children :: Ord v => RDAG v -> v -> Set v
children RDAG{..} = (`SetMultimap.lookup` arcs dg)

reachableSet :: forall v. Ord v => RDAG v -> v -> Set v
reachableSet RDAG{..} = level . Set.singleton
  where
    level :: Set v -> Set v
    level = fix $ \me froms ->
        if Set.null froms
        then froms
        else let thisLevel = mconcat . Set.toList $ Set.map (`SetMultimap.lookup` arcs dg) froms
             in
                 thisLevel <> me thisLevel

-- | An error that prevents adding a new arc to a RDAG
data ValidationError   -- | The new arc would introduce a cycle
    = Cycle
      -- | The new arc would be redundant because its target is already reachable from its source
    | Redundant

isReachable :: Ord v => RDAG v -> Arc v -> Bool
isReachable RDAG{..} Arc{..} = target `Set.member` descendants source

addArc :: Ord v => RDAG v -> Arc v -> Either ValidationError (RDAG v)
addArc rdag arc
    | reachabilityTest arc = Left Redundant
    | reachabilityTest (Arc.reverse arc) = Left Cycle
    | otherwise = case rdag of
        RDAG{..} -> Right . mkRDAG . blowAwayShortcuts $ DG.addArc dg arc
  where
    reachabilityTest = isReachable rdag

    blowAwayShortcuts dg = dg & arcsLens
        %~ (\arcs -> Set.foldr (uncurry SetMultimap.deleteWithValue) arcs possibleShortcuts)
      where
        possibleShortcuts = Set.cartesianProduct (ancestors rdag $ source arc) . descendants rdag $ target arc