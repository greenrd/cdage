# Build dependency docker image to use as a base in .gitlab-ci.yml

FROM haskell:8.10.1

LABEL maintainer="greenrd@greenrd.org"

RUN mkdir /prebuild /builds
RUN ln -s /prebuild /builds/greenrd

WORKDIR /builds/greenrd/cdage

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
RUN useradd -g users -M -N builder
COPY stack.yaml Setup.hs cdage.cabal LICENSE ./
RUN chown -R builder:users ..
ENV HOME=/builds/greenrd

USER builder
RUN mkdir ~/.ssh
RUN ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
RUN stack config set system-ghc --global true
RUN stack setup
RUN stack install --only-dependencies
RUN stack install hlint shelltestrunner
USER root